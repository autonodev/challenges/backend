# Desafio: Autono.dev Backend

Olá! Seja bem-vindo ao desafio de back end da autono.dev.

Nesse desafio, você deverá criar um serviço HTTP
para implementar um desafio a ser descrito abaixo, a implementação deve ser feita em um banco de dados chave-valor, implementado pelo próprio desenvolvedor.

## Desafio

- Dado uma lista de empréstimos e de investimentos, queremos gerar uma nova lista para exibir os empréstimos no marketplace.
- Cada item do marketplace contem os seguintes dados (id, totalRequestedAmount, expiresAt, category, totalInvestedAmount)
- Cada emprestimo é composto de (id, totalRequestedAmountCents, category, expiresAt)
- Cada investimento (id, totalInvestedAmountCents, loanId)
- A lista do marketplace deve ser ordenada por categoria e por data de expiração. Ou seja, digamos que tenhamos 2 emprestimos na msm categoria "X". Queremos priorizar o emprestimo que expirará em breve primeiro.
- As categorias são (X, Y, Z), sendo que seguem a seguinte ordem de prioridade Z > X > Y
- Após a lista ser gerada, armazenar a mesma no banco de dados implementado pelo usuário

MAIN:
export const generateMarketplaceList = (loanList: Loan[], investmentList: Investment[]): MarketplaceItem[] => {}

TYPES:
export enum Category {
X = 'X',
Y = 'Y',
Z = 'Z'
}

export interface Loan {
id: string,
totalRequestedAmountCents: number,
category: Category,
expiresAt: Date
}

export interface Investment {
id: string,
totalInvestedAmountCents: number,
loanId: string
}

export interface MarketplaceItem {
id: string,
totalRequestedAmount: number,
category: Category,
totalInvestmentAmount: number,
expiresAt: Date
}

## Instruções adicionais

As operações disponibilizadas pelo serviço
devem oferecer as melhores garantias de complexidade de tempo possíveis, tornando um bom diferencial implementar uma boa estrutura de dados capaz de executar funções básicas.

É importante notar que
não é permitido utilizar estruturas de dados e algoritmos prontos,
sejam elas fornecidas pela linguagem de programação
ou por bibliotecas de terceiros;
todas as estruturas de dados e algoritmos utilizados na solução final
devem ser escritas por você.

Também não é permitido o uso de bancos de dados.
Nesse desafio, o objetivo é criar um servidor
que, de fato, implementa estruturas de dados e algoritmos
que, juntos, são capazes de oferecer cumprir o desafio acima listado.

## Qual _Stack_ Utilizar

Você tem liberdade total para definir sua _stack_ de desenvolvimento.
Porém, documente o passo-a-passo necessário para executar a aplicação.
Se for possível,
ao invés do passo-a-passo,
você pode criar um arquivo `docker-compose.yml`
que permita subir a aplicação com um simples `docker-compose up`.

## Critérios de Avaliação

Os seguintes critérios serão avaliados:

- cumprimento dos requisitos;
- abordagem usada no levantamento de requisitos e extração de tarefas;
- arquitetura;
- clareza e manutenibilidade do código;
- desempenho e eficiência das operações;
- presença e completude de testes unitários e/ou de integração;
- documentação;
- histórico do Git.

São diferenciais, mas não necessários:

- integração contínua;
- fuzzing;
- disponibilização da aplicação utilizando Docker;
- comentários sobre as decisões técnicas tomadas.

---
